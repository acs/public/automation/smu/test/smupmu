%%  PMU - Measurement compliance test and evaluation
%   IEEE/IEC 60255-118-1-2018
%   https://ieeexplore.ieee.org/document/8577045
%
%   Steady-state compliance
%   -> Out-of-band interference
%   Reference
%   IEC/IEEE 60255-118-1:2018, pag.23, eq. (18)

%% Init

clear, clc
addpath('../libs')

t = initTCP('134.130.169.27', 7080);
p = initSMU(1e4, 10, 'sync_pps', 'mode_freerun');
s = initDAQ(1e6, 'Dev1', p, 'debug_pps');

fprintf('Static out-of-band interference test\n');

%% Test procedure

Xm = 5;                                                                    % Nominal amplitude [V]
f0 = 50;                                                                   % Nominal frequency [Hz]
df = -5:1:5;                                                               % Test frequencies [Hz]
oob= f0+[min(df)-0.1*2.^(8:-1:0),max(df)+0.1*2.^(0:8)];                    % Out of band frequencies [Hz]
T  = 10;                                                                   % Test duration [s]
createStatic(s,T,f0,Xm,0);                                                 % Memory pre-allocation

confSMU(t, s);
sendSMU(t, '[Start]', 'keepalive');
pause(0.5)

for i = 1:numel(df)
  for k = 1:numel(oob)
    fprintf('Testing interference:\t%4.1f Hz + %4.1f Hz\n',f0+df(i),oob(k));
    createStatic(s,T,[f0+df(i),oob(k)],[Xm,Xm/10],[0,0]);                  % Test signal

    outputSingleScan(s,[Xm,0]);
    queueOutputData(s, real(s.UserData.dac));                              % Queue data in the output buffer
    pause(0.1);
    
    m2ep(s,T); startBackground(s); s.wait();
    while(t.UserData.sample_cnt < t.UserData.sample_tot)
        pause(0.5);
    end
    
    % Retrieve data
    test = readSMU(t, s);
    test.f0 = f0+df(i);
    test.oob= oob(k);
    test.T = T;
    data(i,k) = test;
     
    % Reset buffer
    t.UserData.sample_cnt = 0;
    t.UserData.buff_pos   = 0;
  end
end
outputSingleScan(s,[0,0]);
fprintf('Done\n');

sendSMU(t, '[Stop]');
sendSMU(t, '[Exit]');

%% Plot

test = data(21);

figure
plot(test.t, test.smu,...
     test.t, real(test.daq));
xlabel('Time [s]')
ylabel('Voltage [V]')
legend('SMU', 'DAQ')

plotDebugSMU(test);
% plotDebugSPI(s);

%% Save

save('data_interference.mat','data')

