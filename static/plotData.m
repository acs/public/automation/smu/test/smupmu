%% Loading Data
clear all,close all, clc
load data
% data = data(10);
addpath('../libs')

%% Data pre-processing

T = 10;
F = 50;
Fs= data.Fs.smu;
N = Fs/F;
t = (0:1/Fs:1-1/Fs)';

x = data.daq(:,1);
y = data.smu;
xV = repmat(x(1:Fs),1,T,8);
yV = reshape(y,Fs,T,8);
% plotTVE(data)

%% Phasor processing using DFT

tve = @(m,r)abs((m-r)./r);

e_iwt = 2*exp(2i*pi*(0:N-1)/N)';
e_iwV = repmat(e_iwt,1,T,8);

h  = hann(N)/sum(hann(N));
hV = repmat(h,1,T,8);

XV = xV.*0;
YV = yV.*0;

for i=1:Fs-N
    XV(i,:,:) = sum(xV(i:i+N-1,:,:).*e_iwV.*hV,1)/N;
    YV(i,:,:) = sum(yV(i:i+N-1,:,:).*e_iwV.*hV,1)/N;
end

XV = XV(1:end-N-1,:,:);
YV = YV(1:end-N-1,:,:);
tP =  t(1:end-N-1);
M = T;
%% Error simulation

eP = -4429e-6*2;          % AAF phase error   [urad]
eH = -9.81e-6;          % AAF gain  error   [ppm]
eG = -4459e-6;          % ADC gain error    [ppm]
eR = -18.02e-6;         % PWM gain error    [ppm]
eT = 4e-6;             % PLL phase error   [us]

A = (1 + eH)*exp(1i*eP);
G = 1 + eG;
R = repmat(exp(1i*2*pi*F*tP*eR),1,M,8);
T = exp(1i*2*pi*F*eT);
K = A*G*R*T;

uP = -255e-6;           % AAF phase uncertainty   [uV]
uH = -1.13e-6;          % AAF gain uncertainty    [uV]
uG = -134e-6;           % ADC gain uncertainty    [ppm]
uR = -3.67e-6;          % PWM gain uncertainty    [ppm]
uT = -0.7e-6;           % PLL phase uncertainty   [us]

u_A = (1 + uH)*exp(1i*uP);
u_G = 1 + uG;
u_R = repmat(exp(1i*2*pi*F*tP*uR),1,M,8);
u_T = exp(1i*2*pi*F*uT);
u_K = u_A*u_G*u_R*u_T;

uTVEs = tve(K(:,1,1),1)*100;            % Simulated uncompensated TVE
cTVEs = tve(u_K(:,1,1),1)*100;          % Simulated compensated TVE (residual uncertainty)

%% Uncompensated TVE

uTVEm = tve(YV,XV)*100;                 % Measured uncompensated TVE
uTVEm = reshape(uTVEm,Fs-N-1,8*M);

figure
hold on
p1 = plot(tP,uTVEm,'Color',[1,1,1]*0.8);
p2 = plot(tP,mean(uTVEm,2),'k');
p3 = plot(tP,uTVEs,'r-');
hold off

legend([p1(1) p2 p3],{'Measured','Mean','Model'})
legend('Location','Southeast')

xlabel('Time (s)')
ylabel('TVE (%)')

%% Compensated TVE

cTVEm = tve(YV./K,XV)*100;              % Measured compensated TVE
cTVEm = reshape(cTVEm(:,1:M,:),Fs-N-1,8*M);

figure
hold on
p1 = plot(tP,cTVEm,'Color',[1,1,1]*0.8);
p2 = plot(tP,mean(cTVEm,2),'k');
p3 = plot(tP,3.3*cTVEs,'r');
p4 = plot(tP,mean(cTVEm,2)+3.3*std(cTVEm,[],2),'r--');

hold off

legend([p1(1) p2 p3 p4],{'Residual','Mean','Worst Case','Coverage'})
legend('Location','Northeast')

xlabel('Time (s)')
ylabel('TVE (%)')