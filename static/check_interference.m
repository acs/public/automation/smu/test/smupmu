%% Init

clear, clc
addpath('../libs')
load data_interference

%% Ip-mSDFT

N = numel(data);
L = numel(data(1).t);
TVE = zeros(L,N);
FE  = zeros(L,N);
RFE = zeros(L,N);

parfor i=1:N
    fprintf('Pre-processing data: %2d/%2d\n',i,N);
    X(i) = Ip_mSDFT(data(i),'');
end
save('X_interference.mat','X','L','N')

%%
load X_interference.mat
load data_interference

tve = @(m,r)abs((m-r)./r)*100;                                              % Total Vector Error
fe  = @(m,r)abs(m-r);                                                       % Frequency Error
rfe = @(m,r)abs(m-r);                                                       % ROCOF Error

TVE = zeros(L,N);
FE  = zeros(L,N);
RFE = zeros(L,N);

Fr = [10, 25, 50, 100];
Fs = data(1).Fs.smu;
Ts = 1/Fs;
M = 610;
m = reshape([nan(M,1);(M+1:Fs-M)';nan(M,1)]+(0:Fs:L-Fs),L,1);
% m = (1:L)';
% m = [nan(M,1);(M+1:L-M)';nan(M,1)];
f = @(x)mean(x);

for i=1:N
  fprintf('Post-processing data: %2d/%2d\n',i,N);
  t = data(i).t;
  df = data(i).f0-50;
  TVE(:,i) = tve(2*X(i).mX,5);%X(i).rX);
  FE (:,i) = fe (X(i).f,50+df);
  RFE(:,i) = rfe(X(i).df,df);
  
  for k = 1:numel(Fr)
    r = Fs/Fr(k);
    tr= t(1:r:end);
    
    mr = m(1:r:end);
    TVEr(i,k) = f(TVE(mr(~isnan(mr)),i));
    FEr (i,k) = f(FE (mr(~isnan(mr)),i));
    RFEr(i,k) = f(RFE(mr(~isnan(mr)),i));
  end
  
  TVEr(i,5) = f(TVE(~isnan(m),i));
  FEr (i,5) = f(FE (~isnan(m),i));
  RFEr(i,5) = f(RFE(~isnan(m),i));
  
  Interference(i,1) = data(i).oob;
end

report = table(Interference,TVEr,FEr,RFEr);
max(TVEr)
max(FEr)*1e3
max(RFEr)
