%%  PMU - Measurement compliance test and evaluation
%   IEEE/IEC 60255-118-1-2018
%   https://ieeexplore.ieee.org/document/8577045
%
%   Steady-state compliance
%   -> Voltage/Current Magnitude
%   Reference
%   IEC/IEEE 60255-118-1:2018, pag.23, eq. (12)

%% Init

clear, clc
addpath('../libs')

t = initTCP('134.130.169.27', 7080);
p = initSMU(1e4, 10, 'sync_pps', 'mode_freerun');
s = initDAQ(1e6, 'Dev1', p, 'debug_pps');

fprintf('Static magnitude test\n');

%% Test procedure

Xm = 5;                                                                    % Nominal amplitude [V]
Xin= Xm*(0.1:0.1:1.9);                                                     % Test magnitudes [V]
f0 = 50;                                                                   % Nominal frequency [Hz]
T  = 10;                                                                   % Test duration [s]
createStatic(s,T,f0,Xm,0);                                                 % Memory pre-allocation

confSMU(t, s);
sendSMU(t, '[Start]', 'keepalive');
pause(0.5)

for i = 1%:numel(Xin)
    fprintf('Testing magnitude:\t%4.1f %%\n',Xin(i)*100/5);
    createStatic(s,T,f0,Xin(i),0);                                         % Test signal at frequency f0, magnitude Xin and phase 0
    
    outputSingleScan(s,[Xin(i),0]);
    queueOutputData(s, real(s.UserData.dac));                              % Queue data in the output buffer
    pause(0.1);
    
    m2ep(s,T); startBackground(s); s.wait();
    while(t.UserData.sample_cnt < t.UserData.sample_tot)
        pause(0.5);
    end
    
    % Retrieve data
    test = readSMU(t, s);
    test.Xm = Xin(i);
    test.T = T;
    data(i) = test;
    
    % Reset buffer
    t.UserData.sample_cnt = 0;
    t.UserData.buff_pos   = 0;
end
outputSingleScan(s,[0,0]);
fprintf('Done\n');
return

sendSMU(t, '[Stop]');
sendSMU(t, '[Exit]');

%% Plot

% test = data(10);
% 
% figure
% plot(test.t, test.smu(:,1),...
%      test.t, test.daq);
% xlabel('Time [s]')
% ylabel('Voltage [V]')
% legend('SMU', 'DAQ')
% 
% plotDebugSMU(test);
% % plotDebugSPI(s);

%% Save

save('data_magnitude2.mat','data')

