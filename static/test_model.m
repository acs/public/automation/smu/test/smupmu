% clear, close all, clc
addpath('../libs')
tve = @(m,r)abs((m-r)./r)*100;                                              % Total Vector Error
fe  = @(m,r)abs(m-r);                                                       % Frequency Error
rfe = @(m,r)abs(m-r);                                                       % ROCOF Error

T  = 10;
Ts = 1e-4;
Fs = 1/Ts;
Ns = 1e4;

t0 = (0:Ns-1)'*Ts*(1-18e-6);
ti = (0:Ts:T-Ts)';
tr = [];

d  = (rand(1,T)+1)*4e-6;

for i=1:T
    tr = [tr; t0+(i-1)+d(i)-28e-6];
end


f0 = 50;
data.Fs.smu = Fs;
data.t = ti;
data.daq = exp(2i*pi*f0*ti);
data.smu = cos(2*pi*f0*tr);
% plot(ti,[real(data.daq),data.smu],'.-')

% data.smu1=data.smu;
% for i=1:Ns*T-1
%     data.smu(i) = (data.smu(i+1)-data.smu(i))*(mod(i-1,Ns))*18e-6/Ns/Ts+data.smu(i);
% end

X = Ip_mSDFT(data,'');
p=plot(tve(2*X.mX,X.rX)),ylim([0 2])
% plot( fe(2*X.mX,X.rX)),ylim([0 5e-3])
%%
plot([abs(2*X.mX),abs(X.rX)])
plot([angle(X.mX),angle(X.rX)])