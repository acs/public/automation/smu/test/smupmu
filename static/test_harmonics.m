%%  PMU - Measurement compliance test and evaluation
%   IEEE/IEC 60255-118-1-2018
%   https://ieeexplore.ieee.org/document/8577045
%
%   Steady-state compliance
%   -> Harmonic distortion
%   Reference
%   IEC/IEEE 60255-118-1:2018, pag.23, eq. (15); pag.47, Table C.1

%% Init

clear, clc
addpath('../libs')

t = initTCP('134.130.169.27', 7080);
p = initSMU(1e4, 10, 'sync_pps', 'mode_freerun');
s = initDAQ(1e6, 'Dev1', p, 'debug_pps');

fprintf('Static harmonic test\n');

%% Test procedure

Xm = 5;                                                                    % Nominal amplitude [V]
f0 = 50;                                                                   % Nominal frequency [Hz]
fh = f0*(1+(1:50));                                                        % Test harmonics frequency [Hz]
ph = mod(2/3*pi*(1:50),2*pi);                                              % Test harmonics phase [rad]
T  = 10;                                                                   % Test duration [s]
createStatic(s,T,f0,Xm,0);                                                 % Memory pre-allocation

confSMU(t, s);
sendSMU(t, '[Start]', 'keepalive');
pause(0.5)

for i = 1:numel(fh)
    fprintf('Testing harmonic:\t(%2d) %3d Hz\n',i,fh(i));
    createStatic(s,T,[f0,fh(i)],[Xm,Xm/10],[0,ph(i)]);                     % Test signal at frequency fin and magnitude Xm
    
    outputSingleScan(s,[Xm,0]);
    queueOutputData(s, real(s.UserData.dac));                              % Queue data in the output buffer
    pause(0.1);
    
    m2ep(s,T); startBackground(s); s.wait();
    while(t.UserData.sample_cnt < t.UserData.sample_tot)
        pause(0.5);
    end
    
    % Retrieve data
    test = readSMU(t, s);
    test.fh = fh(i);
    test.ph = ph(i);
    test.T = T;
    data(i) = test;
    
    % Reset buffer
    t.UserData.sample_cnt = 0;
    t.UserData.buff_pos   = 0;
end
outputSingleScan(s,[0,0]);
fprintf('Done\n');


sendSMU(t, '[Stop]');
sendSMU(t, '[Exit]');

%% Plot

% test = data(10);
% 
% figure
% plot(test.t.smu, test.smu(:,1),...
%      test.t.smu, test.daq);
% xlabel('Time [s]')
% ylabel('Voltage [V]')
% legend('SMU', 'DAQ')
% 
% plotDebugSMU(test);
% % plotDebugSPI(s);

%% Save

save('data_harmonics.mat','data')

