function [dac] = createDynamic_sweep(s,T,F,Xm,Fm,kx,ka)
%createDynamic_sweep Generates DAC signals for a dynamic test by 
%sweeping cosine magnitude and phase with the specified parameters
%   @s      obj, DAQ session object
%           inf, Continuous test
%   @T      value, Duration of the test in seconds
%   @F      value, Frequency of the test signal
%   @Xm     value, Magnitude of the test signal
%   @Fm     value, Modulating frequency
%   @kx     value, Amplitude modulation factor
%   @ka     value, Angle modulation factor
%

if (F < 0.1)
    error('Valid signal frequency values are > 100 mHz');
elseif (F > s.UserData.Fs.smu/10)
    error(['Valid signal frequency values are < ' num2str(s.UserData.Fs.smu/10/1e3) ' kHz']);
end

if (Fm < 0.1)
    error('Valid modulating frequency values are > 100 mHz');
elseif (Fm > 5)
    error('Valid modulating frequency values are < 5 Hz');
end

if (T < 1e-2)
    error('Valid signal duration values are > 10 ms');
elseif ((T > 10) && ~isinf(T))
    error('Valid signal duration values are < 10 s');
end

if (isinf(T))
    if (s.UserData.mode.sval ~= "mode_freerun")
        error('SMU continuous acquisition requires ''mode_freerun''');
    end
    T = 1;
end

if (Xm < 0.1)
    error('Valid signal magnitude values are > 100 mV');
elseif (Xm > 10)
    error('Valid signal magnitude values are < 10 V');
end

if (kx < 0) || (ka < 0)
    error('Valid modulating factors values are > 0');
end

Fs = s.UserData.Fs;

Fs.ratio = Fs.daq/Fs.smu;                                                  % Sampling frequency ratio

Ts.daq = 1/Fs.daq;                                                         % DAQ Sampling period
Ts.smu = 1/Fs.smu;                                                         % SMU Sampling Period

t.daq = (0:Ts.daq:T-Ts.daq)';                                              % DAQ Timebase
t.smu = (0:Ts.smu:T-Ts.smu)';                                              % SMU Timebase

L.daq = length(t.daq);                                                     % DAQ Number of samples
L.smu = length(t.smu);                                                     % SMU Number of samples

dac.ao = Xm*(1+kx*cos(2*pi*Fm*t.daq)).*...
         exp(2i*pi*F*t.daq+1i*ka*cos(2*pi*Fm*t.daq-pi));                   % Test signal
dac.do = repmat([ones(1e3,1);zeros(Fs.daq-1e3,1)],ceil(T),1);              % PPS  signal  
dac.do = dac.do(1:L.daq);

s.UserData.t = t;
s.UserData.L = L;
s.UserData.Fs = Fs;
s.UserData.dac = [dac.ao dac.do];
s.Rate = Fs.daq;                                                           % DAQ rate
end