function [tcp] = initTCP(ip,port)
%initTCP Init TCP client object
%   Detailed explanation goes here

tcp = tcpip(ip, port, 'NetworkRole', 'client');
tcp.InputBufferSize = 2^20;%2048;
tcp.BytesAvailableFcnMode = 'byte';
% tcp.BytesAvailableFcnCount = 512;
tcp.BytesAvailableFcn = @readTCP;
end

