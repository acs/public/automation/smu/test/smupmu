function plotDebugSPI(s)
%plotDebug Plot SPI debug signals
%   @s      obj, DAQ session object

if(s.UserData.debug == "adc")
    t = s.UserData.t.daq;
    D = s.UserData.data;
    
    figure
    subplot(3,1,1)
    plot(t, D(:,2))
    ylabel('BUSY')

    subplot(3,1,2)
    plot(t, D(:,3))
    ylabel('CS')

    subplot(3,1,3)
    plot(t, D(:,1))
    ylabel('MISO')
end
end

