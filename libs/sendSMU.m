function sendSMU(t, cmd, varargin)
%sendSMU Send TCP based command
%   @t      obj, TCP session object
%   @cmd    char, SMU Command string

if (nargin == 3)
    arg = varargin{1};
    if ((arg ~= "default") && (arg ~= "keepalive"))
        error('Valid post-command parameters are ''default'', ''keepalive''.');
    end
else
    arg = "default";
end


if (t.Status == "closed")
    fopen(t);
end

fwrite(t, cmd, 'char');

if (arg == "default")
    fclose(t);
end
end

