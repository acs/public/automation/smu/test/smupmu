function [] = m2ep(s, T)
%m2ep Measure Two Edges Pulse
%   @s      obj, DAQ session object
%   @T      int, Number of measurements

switch s.UserData.debug
    case 'debug_pps'
        calllib('m2ep_lib','m2ep_init',T);
        s.UserData.T = T;
    otherwise
end

end

