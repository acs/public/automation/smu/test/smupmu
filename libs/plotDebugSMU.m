function plotDebugSMU(data)
%plotDebugSMU Plot derivative based SMU acquisition debug
%   @s      obj, DAQ session object

t = data.t;
D = diff(data.smu(:,1));
D = D/max(D);

figure
plot(data.t,[D;0],...
     data.t, real(data.daq(:,2)))
xlabel('Time [s]')
end

