function [dac] = createLinear(s,SR)
%createLinear Generates DAC signals for a ramp test
%   @s      obj, DAQ session object
%   @SR     value, Slew-rate of the test


if (s.UserData.sync.mode == "sync_pps")
    error('Voltage sweep test supports only sync mode ''sync_debug''.');
end

if (s.IsContinuous)
    s.IsContinuous = false;
    warning('DAQ continuos mode detected, changing to singleshot');
end

Fs = s.UserData.Fs;
T = 2*10/SR;                                                               % Sweep time

Fs.ratio = Fs.daq/Fs.smu;                                                  % Sampling frequency ratio
Ts.daq = 1/Fs.daq;                                                         % DAQ Sampling period
Ts.smu = 1/Fs.smu;                                                         % PMU Sampling Period
t.daq = (0:Ts.daq:T-Ts.daq)';                                              % DAQ Timebase
t.smu = (0:Ts.smu:T-Ts.smu)';                                              % PMU Timebase
L.daq = length(t.daq);
L.smu = length(t.smu);


dac.ao = 9*linspace(-1,1,L.daq)';                                          % -9/9 Ramp signal
dac.do = repmat([ones(Fs.ratio-2,1);0;0],L.smu+1,1);                       % CONV signal  
dac.do = dac.do(1:L.daq);

s.UserData.t = t;
s.UserData.L = L;
s.UserData.Fs = Fs;
s.UserData.dac = [dac.ao dac.do];
s.Rate = Fs.daq;                                                           % DAQ rate
end