function [Xh] = DFT(data,M)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
%% Discrete Fourier Transform (DFT)

x = data.smu;
Fs= data.Fs.smu;
f0= 50;
N = length(x);

K = f0*M/Fs;
h = hann(M);   h = h/sum(h);                                                % Normalized Hanning window
Xh= zeros(N,1);
wM= exp(-2i*pi*K.*(0:M-1)'/M);                                              % Complex twiddle factor

cal = getCal(f0,Fs);                                                        % Model based calibration vector

for n = M:N-1
    Xh(n+1,1) = sum(x(n-M+1:n).*wM.*h)/cal(mod(n,Fs)+1);                   % Phasor with hann window
end

end