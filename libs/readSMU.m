function [data] = readSMU(t, s)
%readPMU Read the PMU data
%   @t      obj, TCP session object
%   @s      obj, DAQ session object

% data = s.UserData;
data.smu = double(reshape(t.UserData.buff_data, 8, t.UserData.sample_cnt))'/2^15*10;
data.smu = data.smu(:,1);
data.daq = s.UserData.dac(1:s.UserData.Fs.ratio:end,:);
data.Fs = s.UserData.Fs;
data.t = s.UserData.t.smu;

switch s.UserData.debug
    case 'debug_pps'
        Td = libpointer('doublePtr',zeros(1,s.UserData.T));
        calllib('m2ep_lib','m2ep_stop',Td);
        data.Td = Td.Value;
    otherwise
end

end

