function [dac] = createDynamic_step(s,T,F,Xm,kx,ka)
%createDynamic_ramp Generates DAC signals for a dynamic test by 
%frequency ramp with the specified parameters
%   @s      obj, DAQ session object
%           inf, Continuous test
%   @T      value, Duration of the test in seconds
%   @F      value, Frequency of the test signal
%   @Xm     value, Magnitude of the test signal
%   @kx     value, Amplitude step factor
%   @ka     value, Angle step factor
%

if (F < 0.1)
    error('Valid signal frequency values are > 100 mHz');
elseif (F > s.UserData.Fs.smu/10)
    error(['Valid signal frequency values are < ' num2str(s.UserData.Fs.smu/10/1e3) ' kHz']);
end

if (T < 1e-2)
    error('Valid signal duration values are > 10 ms');
elseif ((T > 10) && ~isinf(T))
    error('Valid signal duration values are < 10 s');
end

if (isinf(T))
    if (s.UserData.mode.sval ~= "mode_freerun")
        error('SMU continuous acquisition requires ''mode_freerun''');
    end
    T = 1;
end

if (Xm < 0.1)
    error('Valid signal magnitude values are > 100 mV');
elseif (Xm > 10)
    error('Valid signal magnitude values are < 10 V');
end

if (kx > 2) || (ka > pi)
    error('Valid step factors values are  A < 2 and P < pi');
end

Fs = s.UserData.Fs;

Fs.ratio = Fs.daq/Fs.smu;                                                  % Sampling frequency ratio

Ts.daq = 1/Fs.daq;                                                         % DAQ Sampling period
Ts.smu = 1/Fs.smu;                                                         % SMU Sampling Period

t.daq = (0:Ts.daq:T-Ts.daq)';                                              % DAQ Timebase
t.smu = (0:Ts.smu:T-Ts.smu)';                                              % SMU Timebase

L.daq = length(t.daq);                                                     % DAQ Number of samples
L.smu = length(t.smu);                                                     % SMU Number of samples

u = [zeros(L.daq/2+L.daq/20,1);ones(L.daq/2-L.daq/20,1)];
dac.ao = Xm*(1+kx*u).*exp(2i*pi*F*t.daq+1i*ka*u);                          % Test signal
dac.do = repmat([ones(1e3,1);zeros(Fs.daq-1e3,1)],ceil(T),1);              % PPS  signal  
dac.do = dac.do(1:L.daq);

s.UserData.t = t;
s.UserData.L = L;
s.UserData.Fs = Fs;
s.UserData.dac = [dac.ao dac.do];
s.Rate = Fs.daq;                                                           % DAQ rate
end