function [ipX] = Ip_mSDFT(data,mode)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here


%% Parameters

Fs= data.Fs.smu;
Ts= 1/Fs;
f0= 50;
M = 600;                                                                    % Window length
Km= f0*M/Fs;                                                                % Index of DFT maximum
Ki= -2:2;                                                                   % Index of DFT with respect to Km
wM= exp(-2i*pi.*(Km+Ki).*(0:M-1)'/M);                                       % Complex twiddle factor

x = data.smu;
t = data.t;
N = numel(x);                                                               % Number of elements

cal = getCal((Km+Ki)*f0/3,Fs);                                              % Model based calibration vector
% cal = ones(Fs,5);
tve = @(m,r)abs((m-r)./r)*100;                                              % Total Vector Error

%% Reference Phasor

R = data.daq(:,1);

%% Interpolated Modulated Sliding SFT (Ip-mSDFT)

dX0 = zeros(1,5);
% mXr = zeros(1,5);
mXr = zeros(N,numel(Ki));
mX0 = zeros(1,5);
mXh = zeros(N,numel(Ki));
f0h = zeros(N,1);
A0h = zeros(N,1);
p0h = zeros(N,1);
% ipX = zeros(N,1);

m = M;
for n = 1:N
    if n<N
        x(n) = (x(n+1)-x(n))*(mod(n-1,Fs))/Fs*18e-6/Ts+x(n);
    end
%     if n>1
%         x(n) = (x(n)-x(n-1))*(mod(n-1,Fs))*18e-6+x(n);
%     end
    if (n<M+1)
        x0 = 0;
    else
        x0 = x(n-M);
    end
    dx  = (x(n) - x0)/M;                                                    % Input difference
    m = mod(m,M)+1;
    
    for K = Km+Ki
        k = mod((m-1)*K,M)+1;
        
        dX0(K) = wM(k,1)*dx;                                                % Modulated input difference
        mX0(K) = mX0(K) + dX0(K);                                           % Modulated Phasor
%         mXr(n,K) = mX0(K)*conj(wM(mod(k+K-1,M)+1,1));                       % Phasor with rect window (mSDFT)
        mXr(n,K) = mX0(K)*conj(wM(k,1));                                    % Phasor with rect window (mSDFT)
        mXr(n,K) = mXr(n,K)/cal(mod(n,Fs)+1,K);
    end
    for K = Km-1:Km+1
        mXh(n,K) = sum(mXr(n,K-1:K+1).*[-0.25,0.5,-0.25],2);
    end
    eh = sign(abs(mXh(n,Km+1))-abs(mXh(n,Km-1)));
    ah = abs(mXh(n,Km))./abs(mXh(n,Km+eh));
    dh = eh*(2-ah)/(1+ah);
    f0h(n,1) = (Km + dh)/(M*Ts);
    A0h(n,1) = 2*abs(mXh(n,Km))*(pi*dh*(1-dh^2))/sin(pi*dh);
    p0h(n,1) = angle(mXh(n,Km))-pi*dh;
%     ipX(n,1) = A0h(n).*exp(1i*p0h(n));

%% Enhancement
%     B = 1;
%     Wr=@(k1)(exp(-1i*pi*k1*(N-1)/N)*sin(pi*k1)/sin(pi*k1/N));
%     Wh=@(k1)(-0.25*Wr(k1-1)+0.5*Wr(k1)-0.25*Wr(k1+1));
%     V1= (A0h(n,1)/2i)*exp(1i*p0h(n,1));
%     G = conj(V1)*Wh(2*Km+dh)/B;
%     O = conj(V1)*Wh(2*Km+dh+eh)/B;
%     ah = abs(mXh(n,Km)-G)./abs(mXh(n,Km+eh)-O);
%     ah = abs(V1*Wh(-dh))./abs(V1*Wh(eh-dh));
%     dh = eh*(2-ah)/(1+ah);
%     f0h(n,1) = (Km + dh)/(M*Ts);
%     A0h(n,1) = 2*abs(mXh(n,Km))*(pi*dh*(1-dh^2))/sin(pi*dh);
%     p0h(n,1) = angle(mXh(n,Km))-pi*dh;
end
f0h = filter(hann(M)/sum(hann(M)),1,f0h);
ipX.A = circshift(A0h,-M/2);
ipX.p = circshift(p0h,-M);
ipX.mX= ipX.A.*exp(1i*ipX.p);%exp(1i*(ipX.p-2*pi*f0*t));%
ipX.f = circshift(f0h,-M);
% ipX.f = circshift(filter(ones(M,1)/M,1,f0h),-M);
% ipX.df= circshift([0;diff(f0h)/Ts],-M/2);
ipX.df= [0;diff(ipX.f)/Ts];

if (isfield(data,'fh'))
    ipX.rX = 5*exp(2i*pi*f0*t);
else
    ipX.rX = R;
end


switch (mode)
    case 'show'
        %% Discrete Fourier Transform (DFT)

        h = hann(M);   h = h/sum(h);                                        % Normalized Hanning window
        Xh= zeros(N,1);

        for n = M:N-1
            Xh(n+1) = sum(x(n-M+1:n).*wM(:,3).*h)/cal(mod(n,Fs)+1,3);       % Phasor with hann window
        end

        X = ipX.mX;
%         Xh= circshift(abs(Xh),-M/2).*exp(circshift(1i*angle(Xh),-M));

        figure
        plot(t,2*abs(Xh),'-',...
             t,2*abs(X),'-.',...
             t,abs(R.*exp(-2i*pi*f0*t)),'k');
        xlim([M*Ts,data.T-M*Ts])
        xlabel('Time (s)')
        ylabel('Phasor |\cdot|')

        figure
        plot(t,tve(2*Xh,R),'-',...
             t,tve(2*X,R),'-.')
        xlim([M*Ts,data.T-(M+1)*Ts])
        xlabel('Time (s)')
        ylabel('TVE (%)')
    
    otherwise
end
end

