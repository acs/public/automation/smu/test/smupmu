#ifndef M2EP_LIB_H
#define M2EP_LIB_H

#include <windows.h>

__declspec(dllexport) void m2ep_init(int N);
__declspec(dllexport) void m2ep_stop(double *data);

#endif // M2EP_LIB_H
