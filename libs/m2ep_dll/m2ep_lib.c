#include "m2ep_lib.h"
#include <NIDAQmx.h>
#include <pthread.h>

pthread_t thread_id;
TaskHandle  taskHandle=0;
float64 *data;
int N;

void *m2ep_task(void *vargp)
{
    free(data);
    data = calloc(N,sizeof(float64));

    /*********************************************/
    // DAQmx Read Code
    /*********************************************/
    for (int i=0;i<N;i++)
        DAQmxReadCounterScalarF64(taskHandle,-1,&data[i],0);

    return NULL;
}

void m2ep_init(int n)
{
    N = n;

    /*********************************************/
    // DAQmx Configure Code
    /*********************************************/
    DAQmxCreateTask("",&taskHandle);
    DAQmxCreateCITwoEdgeSepChan(taskHandle,"Dev1/ctr0","",0.000000100,0.830000000,DAQmx_Val_Seconds,DAQmx_Val_Rising,DAQmx_Val_Rising,"");

    /*********************************************/
    // DAQmx Start Code
    /*********************************************/
    DAQmxStartTask(taskHandle);
    pthread_create(&thread_id, NULL, m2ep_task, NULL);
    pthread_detach(thread_id);

}

void m2ep_stop(double *p)
{
    memcpy(p,data,N*sizeof(float64));

    if( taskHandle!=0 ) {
        /*********************************************/
        // DAQmx Stop Code
        /*********************************************/
        DAQmxStopTask(taskHandle);
        DAQmxClearTask(taskHandle);
    }
}
