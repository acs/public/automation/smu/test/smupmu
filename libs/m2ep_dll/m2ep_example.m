%% Measure 2-Edges Pulse Example
%  Requires m2ep_lib.dll and m2ep_lib.h

% Number of measurements
N = 10;

% Init measurements
calllib('m2ep_lib','m2ep_init',N);

% Wait for N measurements

% Stop measurements
p = libpointer('doublePtr',zeros(1,N));
calllib('m2ep_lib','m2ep_stop',p);

% Retrieve data
val = p.Value;