function [] = plotTVE(data)
tve = @(m,r)((m-r)./r);
F = 50;
Fs = data.Fs.smu;
N = Fs/F;
T = data.T;

t = (0:1/Fs:T-1/Fs)';
x = data.daq(:,1);
y = data.smu(:,1);

% e_iwt = 2*exp(-2i*pi*F*t);
% X = x.*e_iwt;
% Y = y.*e_iwt;
% 
% 
% for i=1:T*Fs-N
%     XV(i) = sum(X(i:i+N),1)/N;
%     YV(i) = sum(Y(i:i+N),1)/N;
% end

e_iwt = 2*exp(-2i*pi*(0:N-1)/N)';

for i=1:T*Fs-N
    XV(i) = sum(x(i:i+N-1).*e_iwt,1)/N;
    YV(i) = sum(y(i:i+N-1).*e_iwt,1)/N;
end

TVE = tve(YV,XV)*100;

ti = 9*Fs;
tw = 100;
p0 = TVE(ti-200+1)*100;
p1 = TVE(ti+1)*100;

figure
plot(t(1:end-N),abs(TVE),'k')
% yyaxis right
% plot(t(1:end-N),filter(ones(200,1)/200,1,angle(YV)))
xlabel('Time (s)')
ylabel('TVE (%)')

figure
plot(t(ti-tw+1:ti+tw),[x(ti-tw+1:ti+tw) y(ti-tw+1:ti+tw,1)])
xlabel('Time (s)')
ylabel('Amplitude (V)')
legend('Reference','Measured')

figure
compass([p0,p1])

end