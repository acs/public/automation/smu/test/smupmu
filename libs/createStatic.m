function [dac] = createStatic(s,T,F,Xm,P)
%createStatic Generates DAC signals for a static test by composing 
%cosines with the specified parameters
%   @s      obj, DAQ session object
%   @T      value, Duration of the test in seconds
%           inf, Continuous test
%   @F      value, Frequency of the test signal
%   @Xm     value, Magnitude of the test signal
%   @P      value, Phase of the test signal
%

if (F < 0.1)
    error('Valid signal frequency values are > 100 mHz');
elseif (F > s.UserData.Fs.smu/2)
    error(['Valid signal frequency values are < ' num2str(s.UserData.Fs.smu/2/1e3) ' kHz']);
end

if (T < 1e-2)
    error('Valid signal duration values are > 10 ms');
elseif ((T > 10) && ~isinf(T))
    error('Valid signal duration values are < 10 s');
end

if (isinf(T))
    if (s.UserData.mode.sval ~= "mode_freerun")
        error('SMU continuous acquisition requires ''mode_freerun''');
    end
    T = 1;
end

if (Xm < 0.1)
    error('Valid signal magnitude values are > 100 mV');
elseif ((Xm > 10))
    error('Valid signal magnitude values are < 10 V');
end

Fs = s.UserData.Fs;

Fs.ratio = Fs.daq/Fs.smu;                                                  % Sampling frequency ratio

Ts.daq = 1/Fs.daq;                                                         % DAQ Sampling period
Ts.smu = 1/Fs.smu;                                                         % SMU Sampling Period

t.daq = (0:Ts.daq:T-Ts.daq)';                                              % DAQ Timebase
t.smu = (0:Ts.smu:T-Ts.smu)';                                              % SMU Timebase

L.daq = length(t.daq);                                                     % DAQ Number of samples
L.smu = length(t.smu);                                                     % SMU Number of samples

dac.ao = zeros(L.daq,1);
for i = 1:numel(F)
dac.ao = dac.ao + Xm(i)*exp(2i*pi*F(i)*t.daq + 1i*P(i));                   % Test signal
end
dac.do = repmat([ones(1e3,1);zeros(Fs.daq-1e3,1)],ceil(T),1);              % PPS  signal  
dac.do = dac.do(1:L.daq);

s.UserData.t = t;
s.UserData.L = L;
s.UserData.Fs = Fs;
s.UserData.dac = [dac.ao dac.do];
s.Rate = Fs.daq;                                                           % DAQ rate
end