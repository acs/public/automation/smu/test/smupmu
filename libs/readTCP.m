function readTCP(tcp, event)
% disp(['A ' event.Type ' event occurred for ' tcp.Name ' at ' datestr(datenum(eventbuff_data.AbsTime)) '.\n']);

if (tcp.UserData.buff_pos == 0)
    tic;
    fprintf('Test progress %3d%%\n',0);
end

start = tcp.UserData.buff_pos + 1;
stop  = tcp.UserData.buff_pos + 8*tcp.UserData.sample_inc;
tcp.UserData.buff_data(start:stop) = fread(tcp, 8*tcp.UserData.sample_inc, 'int16');
tcp.UserData.buff_pos = stop;

tcp.UserData.sample_cnt = tcp.UserData.sample_cnt + tcp.UserData.sample_inc;
progress = floor(tcp.UserData.sample_cnt/tcp.UserData.sample_tot*100);

fprintf('%c%c%c%c%c%3d%%\n',8,8,8,8,8,progress);

dif = tcp.UserData.sample_tot-tcp.UserData.sample_cnt;

if (dif == 0)
    toc
elseif (dif < tcp.UserData.sample_inc)
    tcp.UserData.sample_inc = dif;
    readTCP(tcp, event);
end
end