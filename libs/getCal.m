function [K] = getCal(F,Fs)
%getCal Calculate calibration complex vector
%   Detailed explanation goes here

Ts = 1/Fs;
t  = (0:Ts:1-Ts)';

R=4.7e3;
C=1.5e-9;
H=@(f)(1./(1+2i*pi*f*4*R*C));

eP = angle(H(F));       % AAF phase error   [urad]
eH = 0*-9.81e-6;          % AAF gain  error   [ppm]
eG = 0*-4459e-6;          % ADC gain error    [ppm]
eR = 0*-18.02e-6;         % PWM gain error    [ppm]
eT = 4e-6;              % PLL phase error   [us]

A = (1 + eH)*exp(1i*eP);
G = 1 + eG;
R = exp(1i*2*pi*F*eR.*t);
T = exp(1i*2*pi*F*eT);
K = A.*G.*R.*T;
end