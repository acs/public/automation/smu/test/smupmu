function [s] = initDAQ(Fs, dev, smu, debug)
%initDAQ Init the DAQ board configuration
%   @Fs_daq value, DAQ timebase frequency
%   @sync   None, no connection
%           PPS,  connect P0.7 to RPi pin XX for PPS signal
%           Debug,connect P0.7 to RPi pin XX for CONV signal
%   @debug  ADC,  connect P0.6(MISO), P0.3(BUSY), P0.2(CS)


s = daq.createSession('ni');
addAnalogOutputChannel(s,dev,'ao0','Voltage');                             % AO

s.UserData.Fs.daq = Fs;                                                    % DAQ sampling frequency
s.UserData.Fs.smu = smu.Fs;                                                % PMU sampling frequency
s.UserData.Fr = smu.Fr;                                                    % PMU reporting rate

%% @sync

s.UserData.sync = smu.sync;

switch smu.sync.sval
    case 'sync_none'

    case 'sync_pps'
        addDigitalChannel(s,dev,'Port0/Line7','OutputOnly');               % PPS
    case 'sync_ntp'

    case 'sync_debug'
        addDigitalChannel(s,dev,'Port0/Line7','OutputOnly');               % CONV
end

%% @mode

s.UserData.mode = smu.mode;

switch smu.mode.sval
    case 'mode_singleshot'

    case 'mode_freerun'

end

%% @debug

s.UserData.debug = debug;

switch debug
    case 'debug_none'
        
    case 'debug_pps'
        lname = 'm2ep_lib.dll';
        hfile = 'm2ep_lib.h';
        if not(libisloaded('m2ep_lib'))
            loadlibrary(lname,hfile);                                      % Two Edges Separation method
        end
    case 'debug_spi'
        addDigitalChannel(s,dev,'Port0/Line6','InputOnly');                % MISO
        addDigitalChannel(s,dev,'Port0/Line3','InputOnly');                % BUSY
        addDigitalChannel(s,dev,'Port0/Line2','InputOnly');                % CS
        addlistener(s,'DataAvailable',@readDAQ);
    otherwise
        error('Valid DAQ debug values are ''debug_none'', ''debug_pps'', ''debug_spi''');   
end

end