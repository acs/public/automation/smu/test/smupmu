%% Init

clear, clc
addpath('../libs')
load data_bandwidth

%% Ip-mSDFT

N = numel(data)/2;
L = numel(data(1).t);
TVE = zeros(L,N);
FE  = zeros(L,N);
RFE = zeros(L,N);

parfor i=1:N
    fprintf('Pre-processing data: %2d/%2d\n',i,N);
    X(i,:) = [Ip_mSDFT(data(i,1),''),Ip_mSDFT(data(i,2),'')];
end
save('X_bandwidth.mat','X','L','N')

%% Magnitude Modulation
load X_bandwidth.mat
load data_bandwidth

tve = @(m,r)abs((m-r)./r)*100;                                              % Total Vector Error
fe  = @(m,r)abs(m-r);                                                       % Frequency Error
rfe = @(m,r)abs(m-r);                                                       % ROCOF Error

TVE = zeros(L,N);
FE  = zeros(L,N);
RFE = zeros(L,N);

Fr = [10, 25, 50, 100];
Fs = data(1).Fs.smu;
Ts = 1/Fs;
M = 610;
m = reshape([nan(M/2,1);(M/2+1:Fs-M)';nan(M,1)]+(0:Fs:L-Fs),L,1);
% m = (1:L)';
% m = [nan(M/2,1);(M/2+1:L-M)';nan(M,1)];
f = @(x)mean(x);

for i=1:N
  fprintf('Post-processing data: %2d/%2d\n',i,N);
  t = data(i).t;
  TVE(:,i) = tve(2*X(i,1).mX,X(i,1).rX);
  FE (:,i) = fe (X(i,1).f,50);
  RFE(:,i) = rfe(X(i,1).df,0);
  
  for k = 1:numel(Fr)
    r = Fs/Fr(k);
    tr= t(1:r:end);
    
    mr = m(1:r:end);
    TVEr(i,k) = f(TVE(mr(~isnan(mr)),i));
    FEr (i,k) = f(FE (mr(~isnan(mr)),i));
    RFEr(i,k) = f(RFE(mr(~isnan(mr)),i));
  end
  
  TVEr(i,5) = f(TVE(~isnan(m),i));
  FEr (i,5) = f(FE (~isnan(m),i));
  RFEr(i,5) = f(RFE(~isnan(m),i));
  
  Bandwidth(i,1) = data(i).fm;
end

report = table(Bandwidth,TVEr,FEr,RFEr);
max(TVEr)
max(FEr)*1e3
max(RFEr)

%% Phase Modulation
load X_bandwidth.mat
load data_bandwidth

tve = @(m,r)abs((m-r)./r)*100;                                              % Total Vector Error
fe  = @(m,r)abs(m-r);                                                       % Frequency Error
rfe = @(m,r)abs(m-r);                                                       % ROCOF Error

TVE = zeros(L,N);
FE  = zeros(L,N);
RFE = zeros(L,N);

Fr = [10, 25, 50, 100];
Fs = data(1).Fs.smu;
Ts = 1/Fs;
M = 610;
m = reshape([nan(M/2,1);(M/2+1:Fs-M)';nan(M,1)]+(0:Fs:L-Fs),L,1);
% m = (1:L)';
m = [nan(M/2,1);(M/2+1:L-M)';nan(M,1)];
f = @(x)mean(x);

for i=1:N
  fprintf('Post-processing data: %2d/%2d\n',i,N);
  t = data(i).t;
  TVE(:,i) = tve(2*X(i,2).mX,X(i,2).rX);
  FE (:,i) = fe (X(i,2).f,50-0.1*(data(i,2).fm)*sin(2*pi*data(i,2).fm*t-pi));
  RFE(:,i) = rfe(X(i,2).df,-0.1*(2*pi*data(i,2).fm^2)*cos(2*pi*data(i,2).fm*t-pi));
  
  for k = 1:numel(Fr)
    r = Fs/Fr(k);
    tr= t(1:r:end);
    
    mr = m(1:r:end);
    TVEr(i,k) = f(TVE(mr(~isnan(mr)),i));
    FEr (i,k) = f(FE (mr(~isnan(mr)),i));
    RFEr(i,k) = f(RFE(mr(~isnan(mr)),i));
  end
  
  TVEr(i,5) = f(TVE(~isnan(m),i));
  FEr (i,5) = f(FE (~isnan(m),i));
  RFEr(i,5) = f(RFE(~isnan(m),i));
  
  Bandwidth(i,1) = data(i).fm;
end

report = table(Bandwidth,TVEr,FEr,RFEr);
max(TVEr)
max(FEr)*1e3
max(RFEr)
