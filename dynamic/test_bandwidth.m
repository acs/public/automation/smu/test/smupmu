%%  PMU - Measurement compliance test and evaluation
%   IEEE/IEC 60255-118-1-2018
%   https://ieeexplore.ieee.org/document/8577045
%
%   Dynamic compliance
%   -> Bandwidth
%   Reference
%   IEC/IEEE 60255-118-1:2018, pag.25, eq. (25)

%% Init

clear, clc
addpath('../libs')

t = initTCP('134.130.169.27', 7080);
p = initSMU(1e4, 10, 'sync_pps', 'mode_freerun');
s = initDAQ(1e6, 'Dev1', p, 'debug_pps');

fprintf('Dynamic bandwidth test\n');

%% Test procedure

Xm = 5;                                                                    % Nominal amplitude [V]
f0 = 50;                                                                   % Nominal frequency [Hz]
fm = 0.1:0.35:5;                                                           % Test frequencies [Hz]
kx = [0.1,0];                                                              % Amplitude modulation factor
ka = [0,0.1];                                                              % Phase modulation factor
T  = 10;                                                                   % Test duration [s]
createDynamic_sweep(s,T,f0,Xm,1,0,0);                                      % Memory pre-allocation

confSMU(t, s);
sendSMU(t, '[Start]', 'keepalive');
pause(0.5)

for i = 1:numel(fm)
    fprintf('Testing A/P modulation:\t%4.1f Hz\n',fm(i));
  for k = 1:2    
    createDynamic_sweep(s,T,f0,Xm,fm(i),kx(k),ka(k));                      % Test signal

    outputSingleScan(s,[Xm,0]);
    queueOutputData(s, real(s.UserData.dac));                              % Queue data in the output buffer
    pause(0.1);
    
    m2ep(s,T); startBackground(s); s.wait();
    while(t.UserData.sample_cnt < t.UserData.sample_tot)
        pause(0.5);
    end
    
    % Retrieve data
    test = readSMU(t, s);
    test.f0 = f0;
    test.fm = fm(i);
    test.T = T;
    data(i,k) = test;
     
    % Reset buffer
    t.UserData.sample_cnt = 0;
    t.UserData.buff_pos   = 0;
  end
end
outputSingleScan(s,[0,0]);
fprintf('Done\n');

sendSMU(t, '[Stop]');
sendSMU(t, '[Exit]');

%% Plot

% test = data(21);
% 
% figure
% plot(test.t, test.smu,...
%      test.t, real(test.daq));
% xlabel('Time [s]')
% ylabel('Voltage [V]')
% legend('SMU', 'DAQ')
% 
% plotDebugSMU(test);
% plotDebugSPI(s);

%% Save

save('data_bandwidth.mat','data')

